from random import randint

# Generates a list of four, unique, single-digit numbers
def generate_secret():
    list = []
    while len(list) < 4:
        n = randint(0,9)
        if n not in list:
            list.append(n)
    return list


def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have a secret four numbers.")
    print("Can you guess it?")
    print("You have 20 tries.")
    print()
  
    secret = generate_secret()
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        # TODO:
        # while the length of their response is not equal
        # to 4,
        #   tell them they must enter a four-digit number
        #   prompt them for the input, again

        while len(guess) != 4:
            print("you must enter a four-digit number")
            guess = input(prompt)


        # TODO:
        # write a function and call it here to:
        # convert the string in the variable guess to a list
        # of four single-digit numbers
        def parse_numbers(s):
            list = []
            for n in s:
                number = int(n)
                list.append(number)
            return list

        converted_guess = parse_numbers(guess)

        # TODO:
        # write a function and call it here to:
        # count the number of exact matches between the
        # converted guess and the secretdef count_exact_matches(first, second):
        def bull(first, second):
            count = 0
            for a, b in zip(first, second):
                if a == b:
                    count = count + 1
            return count

        total_bulls = bull(converted_guess, secret)

        # TODO:
        # write a function and call it here to:
        # count the number of common entries between the
        # converted guess and the secret
        

        def cow(first, second):
            count = 0
            for entry in first:
                if entry in second: 
                    count= count + 1
            return count

        total_cows = cow(converted_guess, secret)


        # TODO:
        # if the number of exact matches is four, then
        # the player has correctly guessed! tell them
        # so and return from the function.

    
        if total_bulls == 4:
            print("you've guessed the right numbers!!")
            return

        # TODO:
        # report the number of "bulls" (exact matches)
        print("total number of bulls: ", total_bulls)
        
        # TODO:
        # report the number of "cows" (common entries - exact matches)
        print("total number of cows: ", total_cows - total_bulls)

    # TODO:
    # If they don't guess it in 20 tries, tell
    # them what the secret was.
    print("the secret was: ", secret)


# Runs the game
def run():
    response = input("Do you want to play a game?")

    # While the person wants to play the game
    #   Call the play_game function
    #   Ask if they want to play again
    # pass
    while response.lower() == "y":
        play_game()
        response = input("Do you want to play a game?")


if __name__ == "__main__":
    run()
